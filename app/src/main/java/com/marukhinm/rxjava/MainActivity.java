package com.marukhinm.rxjava;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    @SuppressLint("CheckResult")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Observable.create((ObservableEmitter<String> emitter) -> {
            Random random = new Random(System.currentTimeMillis());
            while (true) {
                emitter.onNext("Jake Wharton");
                Thread.sleep(random.nextInt(1001));
            }
        })
                .observeOn(AndroidSchedulers.mainThread())
                .map(s -> s.concat(" the best!"))
                .debounce(800, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .subscribe(
                        s -> Log.i("TAG", "Message text: " + s),
                        e -> Log.e("TAG", e.getMessage())
                );
    }
}
